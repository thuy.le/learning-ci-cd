FROM node:14.16.0-alpine

WORKDIR /app 
COPY package.json /app
COPY yarn.lock /app
COPY . /app 
RUN yarn install

CMD [ "node", "index.js" ]